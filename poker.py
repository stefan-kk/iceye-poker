import sys
from classes import Poker

def usage():
    print(f"Usage: python {sys.argv[0]} <hand-1> <hand-2>")

def main():
    if (len(sys.argv) != 3):
        usage()
        exit(0)
    else:
        print(Poker().poker(sys.argv[1],
                            sys.argv[2]))

main()
