import unittest
import pytest

from classes import Poker

class PokerTest(unittest.TestCase):
    test_poker = Poker()

    # decoupled expected result strings
    tie = "It's a tie!"
    first_wins = "First hand wins!"
    second_wins = "Second hand wins!"

    def test_rank(self):
        self.assertEqual(self.test_poker.rank("2"),
                         0)
        self.assertEqual(self.test_poker.rank("3"),
                         1)
        self.assertEqual(self.test_poker.rank("4"),
                         2)
        self.assertEqual(self.test_poker.rank("5"),
                         3)
        self.assertEqual(self.test_poker.rank("6"),
                         4)
        self.assertEqual(self.test_poker.rank("7"),
                         5)
        self.assertEqual(self.test_poker.rank("8"),
                         6)
        self.assertEqual(self.test_poker.rank("9"),
                         7)
        self.assertEqual(self.test_poker.rank("T"),
                         8)
        self.assertEqual(self.test_poker.rank("J"),
                         9)
        self.assertEqual(self.test_poker.rank("Q"),
                         10)
        self.assertEqual(self.test_poker.rank("K"),
                         11)
        self.assertEqual(self.test_poker.rank("A"),
                         12)

    def test_tie(self):
        hands = ["AAAQQ","QQAAA"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)
        hands = ["53QQ2","Q53Q2"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)
        hands = ["53888","88385"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)
        hands = ["QQAAA","AAAQQ"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)
        hands = ["Q53Q2","53QQ2"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)
        hands = ["88385","53888"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.tie)

    def test_first_hand(self):
        hands = ["AAAQQ","QQQAA"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["Q53Q4","53QQ2"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["53888","88375"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["33337","QQAAA"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["22333","AAA58"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["33389","AAKK4"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["44223","AA892"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["22456","AKQJT"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99977","77799"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99922","88866"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["9922A","9922K"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99975","99965"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99975","99974"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99752","99652"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99752","99742"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)
        hands = ["99753","99752"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.first_wins)

    def test_second_hand(self):
        hands = ["QQQAA","AAAQQ"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["53QQ2","Q53Q4"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["88375","53888"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["QQAAA","33337"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["AAA58","22333"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["AAKK4","33389"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["AA892","44223"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["AKQJT","22456"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["77799","99977"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["88866","99922"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["9922K","9922A"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["99965","99975"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["99974","99975"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["99652","99752"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["99742","99752"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)
        hands = ["99752","99753"]
        self.assertEqual(self.test_poker.poker(hands[0], hands[1]),
                         self.second_wins)

if __name__ == '__main__':
    unittest.main()
