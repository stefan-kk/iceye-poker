# Python Programming Assignment

The goal of this assignment is to test your programming skills using Python. We hope to receive an answer that is written in idiomatic Python3 and is easy to read, understand and modify. Use those features of language that make sense to you in this case.

You can share the code with us by e.g. a private gitlab repository or just by zipping up the code and attaching it to an email. We hope to have your answer within a week.

## Specs
Given two poker hands determine which one wins. We use a simplified version of poker, where the cards don't have suits and there are no flushes, straights or straight flushes.

The two hands are given as strings of five characters, where each character is one of 23456789TJQKA. The answer should be "First hand wins!", "Second hand wins!" or "It's a tie!".

A hand wins if it has a more valuable combination than the other or if they have the same combination but the cards of one are higher than the cards of the other.

The combinations are (in order of value):

* four of a kind (e.g. 77377)
* full house (e.g. KK2K2)
* triples (e.g. 32666)
* two pairs (e.g. 77332)
* pair (e.g. 43K9K)
* high card (i.e. anything else, e.g. 297QJ)

When the hands have the same combination, the winner is the one where the combination is formed with higher cards. In the case of two pairs, the higher pair is compared first (e.g. 99662 wins 88776). In the case of full house, the triples are compared first and then the pair (e.g. 88822 wins QQ777). If the combinations are formed with the same cards, then the rest of the cards are compared from the highest card to the lowest. E.g. when the hands are 7T2T6 and TT753, the first one wins because TT=TT, 7=7 and 6>5.

_Examples are skipped here as they can be found in the unit tests (see "Solution" chapter)_

## Solution

The solution is self-contained using a simplified one-level project structure:

1. `poker.py`: Entrypoint to support the specified signature to execute the script.

  ```
  python poker.py 22456 AKQJT
  ```

2. `classes.py`: Implementation of poker logic and helper classes, logging configuration
3. `poker_unittest.py`: Unit tests to run given test cases.

  ```
  python -m pytest poker_unittest.py  
  ```

### Logging

The code has some logging configuration included in `classes.py` which is set to `ERROR`
```
logging.basicConfig(level=logging.ERROR)
```
Debug logging can be enabled by setting the provided logging level to `DEBUG`.
