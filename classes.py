import logging
from enum import Enum, unique
from operator import itemgetter
from itertools import groupby

logging.basicConfig(level=logging.ERROR)

CARD_DECK="23456789TJQKA"

class Poker:
    def poker(self, hand1, hand2):
        '''
        Compare two poker hands and return a specified message accordingly.
        Basic steps are:
        1. Get the order and rank of both given hands
        2. Compare the result according to the specified rules
        3. Return the specified message according the comparison result
        '''
        # first hand
        ordered_hand_1 = self.order(hand1)
        logging.debug(ordered_hand_1)
        first_card_combination = ordered_hand_1[0].value
        first_card_ranks = ordered_hand_1[1]

        # second hand
        ordered_hand_2 = self.order(hand2)
        logging.debug(ordered_hand_2)
        second_card_combination = ordered_hand_2[0].value
        second_card_ranks = ordered_hand_2[1]

        if first_card_combination > second_card_combination:
            return Messages.FIRST_WINS.text
        elif first_card_combination < second_card_combination:
            return Messages.SECOND_WINS.text
        else:
            for i in range(len(first_card_ranks)):
                if first_card_ranks[i] > second_card_ranks[i]:
                    return Messages.FIRST_WINS.text
                elif first_card_ranks[i] < second_card_ranks[i]:
                    return Messages.SECOND_WINS.text
            return Messages.TIE.text

    def order(self, hand):
        '''
        Determines the order of a given poker hand based on matching
        poker combinations (e.g. full house).

        Returns: tuple(combination enum, card ranks) of the given hand
        '''
        hand = sorted(hand, reverse=True)
        groups = [list(group) for i, group in groupby(hand,
                                                      key=itemgetter(0))]
        groups.sort(key=len, reverse=True)
        logging.debug(groups)

        combination = CardsCombinations(tuple(len(group) for group in groups))
        logging.debug(combination.name)
        logging.debug(combination.value)

        ranks = list(self.rank(same[0]) for same in groups)

        return combination, ranks

    def rank(self, card):
        '''
        Determines the rank of a given card based on the provided card deck order.

        Returns: Numeric representation of the given card's rank
        '''
        return CARD_DECK.index(card)

@unique
class Messages(Enum):
    TIE = "It's a tie!"
    FIRST_WINS = "First hand wins!"
    SECOND_WINS = "Second hand wins!"

    def __init__(self, text):
        self.text = text

@unique
class CardsCombinations(Enum):
    FOUR_OF_A_KIND = (4, 1)
    FULL_HOUSE = (3, 2)
    TRIPLES = (3, 1, 1)
    TWO_PAIRS = (2, 2, 1)
    PAIR = (2, 1, 1, 1)
    HIGH_CARD = (1, 1, 1, 1, 1)

    def __init__(self, *combination):
        self.combination = combination

    @property
    def value(self):
        if self.combination == CardsCombinations.FOUR_OF_A_KIND.combination:
            return 5
        elif self.combination == CardsCombinations.FULL_HOUSE.combination:
            return 4
        elif self.combination == CardsCombinations.TRIPLES.combination:
            return 3
        elif self.combination == CardsCombinations.TWO_PAIRS.combination:
            return 2
        elif self.combination == CardsCombinations.PAIR.combination:
            return 1
        else:
            return 0
